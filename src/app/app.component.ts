import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'TestEquity';
  flag: boolean;

  constructor(private http: HttpClient) { }

  listMegaMenu: any[];
  selectedMenu: any;
  graybox0: boolean;
  graybox1: boolean;
  graybox2: boolean;
  graybox3: boolean;
  graybox4: boolean;
  graybox5: boolean;
  graybox6: boolean;
  graybox7: boolean;
  graybox8: boolean;
  graybox9: boolean;
  graybox10: boolean;

  ngOnInit() {
    this.getMenuMock().subscribe((x) => {
      this.listMegaMenu = x;
      this.resetMenu()
    });
    this.flag = false;
    this.setGrayBoxFalse();
    this.graybox0 = true;
  }

  changeMenu(index: number) {
    this.selectedMenu = this.listMegaMenu[index];
    this.setGrayBoxFalse();
    switch (index) {
      case 0:
        this.graybox0 = true;
        break;
      case 1:
        this.graybox1 = true;
        break;
      case 2:
        this.graybox2 = true;
        break;
      case 3:
        this.graybox3 = true;
        break;
      case 4:
        this.graybox4 = true;
        break;
      case 5:
        this.graybox5 = true;
        break;
      case 6:
        this.graybox6 = true;
        break;
      case 7:
        this.graybox7 = true;
        break;
      case 8:
        this.graybox8 = true;
        break;
      case 9:
        this.graybox9 = true;
        break;
      case 10:
        this.graybox10 = true;
        break;
    }
  }

  setGrayBoxFalse() {
    this.graybox0 = false;
    this.graybox1 = false;
    this.graybox2 = false;
    this.graybox3 = false;
    this.graybox4 = false;
    this.graybox5 = false;
    this.graybox6 = false;
    this.graybox7 = false;
    this.graybox8 = false;
    this.graybox9 = false;
    this.graybox10 = false;
  }

  resetMenu() {
    this.selectedMenu = this.listMegaMenu[0];
  }

  getMenuMock(): Observable<any[]> {
    return this.http.get<any[]>('../assets/menuMock.json');
  }

}
